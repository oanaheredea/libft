/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oheredea <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/27 20:51:51 by oheredea          #+#    #+#             */
/*   Updated: 2017/11/27 21:11:21 by oheredea         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t len)
{
	unsigned char *temp;

	temp = (unsigned char*)s;
	while (len--)
	{
		if (*temp++ == (unsigned char)c)
			return (void *)(temp - 1);
	}
	return (0);
}
