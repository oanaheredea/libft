/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oheredea <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/07 13:56:12 by oheredea          #+#    #+#             */
/*   Updated: 2017/12/15 16:32:48 by oheredea         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(char const *s)
{
	char	*cpy;
	int		len;
	int		i;
	int		j;

	i = 0;
	len = ft_strlen(s) - 1;
	j = 0;
	while (len >= 0 && (s[len] == ' ' || s[len] == '\n' || s[len] == '\t'))
		len--;
	if (len < 0)
	{
		cpy = (char *)malloc(1);
		*cpy = '\0';
		return (cpy);
	}
	while (s[i] && (s[i] == ' ' || s[i] == '\n' || s[i] == '\t'))
		i++;
	cpy = (char*)malloc(sizeof(char) * (len - i + 2));
	if (!cpy)
		return (0);
	while (i <= len)
		cpy[j++] = s[i++];
	cpy[j] = '\0';
	return (cpy);
}
