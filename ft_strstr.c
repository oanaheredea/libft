/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oheredea <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/02 19:50:37 by oheredea          #+#    #+#             */
/*   Updated: 2017/12/14 20:11:43 by oheredea         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *big, const char *little)
{
	size_t len;

	len = ft_strlen(little);
	if (!len)
		return ((char *)(big));
	while (*big)
	{
		if (!ft_memcmp(big, little, len))
			return ((char*)(big));
		big++;
	}
	return (0);
}
