/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oheredea <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/02 19:30:56 by oheredea          #+#    #+#             */
/*   Updated: 2017/12/14 20:03:01 by oheredea         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	char *str;
	char ch;
	char *d;

	d = 0;
	ch = (char)c;
	str = (char*)s;
	while (*str)
	{
		if (*str == ch)
			d = str;
		str++;
	}
	if (!c && !*str)
		return (str);
	if (d)
		return (d);
	return (0);
}
