/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oheredea <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/07 15:34:37 by oheredea          #+#    #+#             */
/*   Updated: 2017/12/15 18:56:41 by oheredea         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		count_words(char const **s, char c)
{
	int words;
	int i;

	i = 0;
	words = 0;
	while (**s == c)
		(*s)++;
	while ((*s)[i])
	{
		while (!((*s)[i] == c) && (*s)[i])
			i++;
		words++;
		while (((*s)[i] == c) && (*s)[i])
			i++;
	}
	return (words);
}

static void		ft_if(int *letters, int *i, int *j, char **rez)
{
	if (*letters)
	{
		rez[*j][*letters] = '\0';
		(*j)++;
	}
	else
		(*i)++;
}

static char		**ft_strsplit_aux(char const *s, int words, char c, int i)
{
	char	**rez;
	int		letters;
	int		k;
	int		j;

	j = 0;
	rez = (char**)malloc(words * sizeof(char*) + 1);
	if (!rez)
		return (0);
	while (s[i])
	{
		k = 0;
		letters = 0;
		while (s[i + letters] && !(s[i + letters] == c))
			letters++;
		rez[j] = (char*)malloc(letters * sizeof(char) + 1);
		if (!rez[j])
			return (0);
		while (s[i] && !(s[i] == c))
			rez[j][k++] = s[i++];
		ft_if(&letters, &i, &j, rez);
	}
	rez[j] = 0;
	return (rez);
}

char			**ft_strsplit(char const *s, char c)
{
	int wc;

	if (!s)
		return (0);
	wc = count_words(&s, c);
	return (ft_strsplit_aux(s, wc, c, 0));
}
