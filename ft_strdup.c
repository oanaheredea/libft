/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oheredea <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/02 14:08:43 by oheredea          #+#    #+#             */
/*   Updated: 2017/12/09 18:45:35 by oheredea         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *s1)
{
	size_t		len;
	char		*s2;
	const char	*src;
	char		*new;

	src = s1;
	len = ft_strlen(s1);
	s2 = (char*)malloc((len + 1) * sizeof(char));
	if (!s2)
		return (0);
	new = s2;
	while (len--)
		*new++ = *src++;
	*new = 0;
	return (s2);
}
