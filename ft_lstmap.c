/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oheredea <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/08 17:04:07 by oheredea          #+#    #+#             */
/*   Updated: 2017/12/15 18:49:41 by oheredea         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	add(t_list **lst, t_list *elem)
{
	t_list	*temp;

	if (!*lst)
		*lst = ft_lstnew(elem->content, elem->content_size);
	else
	{
		temp = *lst;
		while (temp->next)
			temp = temp->next;
		temp->next = ft_lstnew(elem->content, elem->content_size);
	}
}

t_list		*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list *new;

	new = 0;
	while (lst)
	{
		add(&new, f(lst));
		lst = lst->next;
	}
	return (new);
}
