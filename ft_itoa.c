/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oheredea <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/07 16:12:35 by oheredea          #+#    #+#             */
/*   Updated: 2017/12/15 18:29:32 by oheredea         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	ft_swap(char *a, char *b)
{
	char aux;

	aux = *a;
	*a = *b;
	*b = aux;
}

static void	ft_reverse(char *str, unsigned int len)
{
	int start;
	int end;

	start = 0;
	end = len - 1;
	while (start < end)
	{
		ft_swap((str + start), (str + end));
		start++;
		end--;
	}
}

static void	ft_while(long *n, int *i, int *rest, char *str)
{
	while (*n)
	{
		*rest = *n % 10;
		str[(*i)++] = *rest + '0';
		*n /= 10;
	}
}

static int	len(long n)
{
	int i;

	i = 0;
	if (n < 0)
		i++;
	while (n /= 10)
		i++;
	return (i);
}

char		*ft_itoa(int nr)
{
	char	*str;
	int		neg;
	int		i;
	int		rest;
	long	n;

	n = nr;
	i = 0;
	neg = 0;
	str = (char*)malloc(sizeof(char) * len(nr) + 2);
	if (!str)
		return (0);
	if (n == 0)
		return (ft_strdup("0"));
	if (n < 0)
	{
		n = -n;
		neg = 1;
	}
	ft_while(&n, &i, &rest, str);
	if (neg == 1)
		str[i++] = '-';
	str[i] = '\0';
	ft_reverse(str, i);
	return (str);
}
