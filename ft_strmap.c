/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oheredea <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/06 15:42:26 by oheredea          #+#    #+#             */
/*   Updated: 2017/12/15 16:16:37 by oheredea         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	char *s2;
	char *s2cpy;

	s2 = (char*)ft_memalloc(ft_strlen(s) + 1);
	if (!s2)
		return (0);
	s2cpy = s2;
	while (*s)
	{
		*s2cpy++ = f(*s++);
	}
	return (s2);
}
