/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oheredea <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/07 13:25:23 by oheredea          #+#    #+#             */
/*   Updated: 2017/12/07 13:43:18 by oheredea         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char	*cpy;
	size_t	strlen;
	int		i;

	i = 0;
	strlen = ft_strlen(s);
	cpy = (char*)malloc(sizeof(char) * (len + 1));
	if (!cpy)
		return (0);
	while (len--)
	{
		cpy[i++] = s[start++];
	}
	cpy[i] = '\0';
	return (cpy);
}
