/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oheredea <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/06 16:19:34 by oheredea          #+#    #+#             */
/*   Updated: 2017/12/15 16:18:36 by oheredea         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	char	*s2;
	char	*s2cpy;
	int		i;

	i = 0;
	s2 = (char*)ft_memalloc(ft_strlen(s) + 1);
	if (!s2)
		return (0);
	s2cpy = s2;
	while (*s)
	{
		*s2cpy = f(i++, *s);
		s++;
		s2cpy++;
	}
	return (s2);
}
