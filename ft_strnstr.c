/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oheredea <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/02 20:17:20 by oheredea          #+#    #+#             */
/*   Updated: 2017/12/14 22:41:25 by oheredea         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *big, const char *little, size_t len)
{
	size_t len2;

	if (!*little)
		return (char*)(big);
	len2 = ft_strlen(little);
	while (*big && len-- >= len2)
	{
		if (!ft_memcmp(big, little, len2))
			return ((char*)(big));
		big++;
	}
	return (0);
}
